<?php


/**
 * @file
 * Theme setting callbacks for the boise theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function boise_form_system_theme_settings_alter(&$form, &$form_state) {

  // Get boise theme path.
  $boise_path = drupal_get_path('theme', 'boise');
  $custom_css_path = $boise_path . '/css/custom.css';

  $form['boise_custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('boise Customization'),
    '#weight' => -10,
  );

  $form['boise_custom']['display_icons_config'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display icons in Configuration page'),
    '#default_value' => theme_get_setting('display_icons_config'),
  );

  $form['boise_custom']['use_custom_media_queries'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Custom Media Queries'),
    '#description' => t('You can override the mobile and tablet media queries from this option. Use it only if you know what media queries are and how to use them.'),
    '#default_value' => theme_get_setting('use_custom_media_queries'),
  );

  $form['boise_custom']['media_queries'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Media Queries'),
    '#states' => array(
      // Hide the settings when the cancel notify checkbox is disabled.
      'visible' => array(
       ':input[name="use_custom_media_queries"]' => array('checked' => TRUE),
      ),
     ),
  );

  $form['boise_custom']['media_queries']['media_query_mobile'] = array(
    '#type' => 'textfield',
    '#title' => t('Mobile media query'),
    '#description' => t('The media query to load the mobile.css styles.'),
    '#default_value' => theme_get_setting('media_query_mobile'),
  );

  $form['boise_custom']['media_queries']['media_query_tablet'] = array(
    '#type' => 'textfield',
    '#title' => t('Tablet media query'),
    '#description' => t('The media query to load the tablet.css styles.'),
    '#default_value' => theme_get_setting('media_query_tablet'),
  );

  $form['boise_custom']['custom_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use "custom.css"'),
    '#description' => t('Include custom.css file to override default boise styles
     or add additional styles without subthememing/hacking boise Theme.'),
    '#default_value' => theme_get_setting('custom_css'),
  );
}
