# README #

This is a fork of of the Adminimal theme called Boise.

### What's different ###

* Separate font stack to make using with your site easier
* By default does not use any external fonts. The default font isn't generally available but it's easy to sub theme and override the font stack with your own. Generally looks best with a Sans font along the lines of Open Sans
* Reduced complexity and removed minimally edited templates
* Fixed broken zooming on mobile devices
* Removed support for ie6